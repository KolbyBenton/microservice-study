import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    message = json.loads(body)
    print(f"Approval email sent to {message['presenter_email']}")
    send_mail(
        "Your presentation has been approved!",
        f"{message['presenter_name']}, we're happy to say your presentation has been approved!",
        "admin@conference.go",
        [message["presenter_email"]],
    )


def process_rejection(ch, method, properties, body):
    message = json.loads(body)
    print(f"Rejection email sent to {message['presenter_email']}")
    send_mail(
        "Your presentation has been rejection!",
        f"{message['presenter_name']}, we're happy to say your presentation has been SIKE rejected!",
        "admin@conference.go",
        [message["presenter_email"]],
    )
